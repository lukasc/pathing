﻿using UnityEngine;
using System.Collections.Generic;

namespace Warcraft.CC {
    public class CCAgentGroup {

        private List<CCAgent> _Agents;
        private CCGroupBuffer _Buffer;
        private Vector2 _Goal;
        private float _Radius;
        private bool _Available;

        public CCGroupBuffer buffer { get { return _Buffer; } }
        public Vector2 goal { get { return _Goal; } set { _Goal = value; } }
        public float radius { get { return _Radius; } }

        public CCAgentGroup() {
            _Agents = new List<CCAgent>();
            _Buffer = new CCGroupBuffer();
            _Available = true;
        }

        public bool Try(CCAgent agent, Vector2 goal, float radius) {
            if (goal == _Goal && radius == _Radius) {
                _Agents.Add(agent);

                return true;
            }

            if (_Available) {
                _Available = false;
                _Goal = goal;
                _Radius = radius;
                _Agents.Add(agent);

                return true;
            }

            return false;
        }

        public void Recalculate() {
            if (_Agents.Count == 0)
                return;

            _Buffer.RecalculateSpeedCost();
            _Buffer.RecalculatePotential((int)_Goal.x, (int)_Goal.y);
            _Buffer.RecalculateFlow();

            int count = _Agents.Count;

            for (int i = 0; i < count; i++) {
                CCAgent agent = _Agents[i];
                //Vector2 velocity = _Buffer.SampleVelocity(agent.transform.position);
                Vector2 velocity = _Buffer.SampleVelocity(agent.transform.position);
                //Debug.Log(velocity);

                Debug.DrawLine(new Vector3((int)agent.transform.position.x, 0, (int)agent.transform.position.z), new Vector3((int)agent.transform.position.x, 0, (int)agent.transform.position.z) + Vector3.up, Color.blue);
                agent.AddVelocity(velocity);
            }
        }

        public Color[] CalculateColors() {
            Color[] colors = new Color[CCManager.main.width * CCManager.main.height];

            for (int j = 0; j < CCManager.main.height; j++) {
                for (int i = 0; i < CCManager.main.width; i++) {
                    CCGroupBufferTile tile = _Buffer.GetTile(i, j);
                    CCMainBufferTile t = CCManager.mainBuffer.GetTile(i, j);

                    colors[CCManager.main.width * j + i] = new Color(tile.potential / 80f, t.density, 0, 0) ;
                }
            }

            return colors;
        }
    }
}
