﻿using UnityEngine;
using System.Collections.Generic;

namespace Warcraft.CC {
    public enum CCGroupBufferTileState {
        Unknown,
        Known,
        Candidate,
    }

    public class CCGroupBufferTile {

        public float[] speeds;
        public float[] costs;
        public float potential;
        public Vector2[] gradient;
        public Vector2 test;
        public CCGroupBufferTileState state;

        public CCGroupBufferTile right;
        public CCGroupBufferTile up;
        public CCGroupBufferTile left;
        public CCGroupBufferTile down;

        public CCGroupBufferTile() {
            gradient = new Vector2[4];
            speeds = new float[4];
            costs = new float[4];
        }

        public void Reset() {
            potential = 1000;
            state = CCGroupBufferTileState.Unknown;
        }
    }

    public class CCGroupBuffer {

        private static Vector2i[] _Directions = { Vector2i.right, Vector2i.up, Vector2i.left, Vector2i.down };

        private CCGroupBufferTile[][] _Tiles;
        private int _Width;
        private int _Height;
        private int _WidthLimit;
        private int _HeightLimit;

        private float _MaxSpeed;
        private float _MinSpeed;
        private float _MaxSlope;
        private float _MinSlope;
        private float _DiffSlope;
        private float _MaxDensity;
        private float _MinDensity;
        private float _DiffDensity;

        private List<CCGroupBufferTile> _Candidates;

        public CCGroupBuffer() {
            RecalculateSize();

            _Candidates = new List<CCGroupBufferTile>(32);
            _MinSpeed = 0.1f;
            _MaxSpeed = 8.0f;
            _MinSlope = -5.0f;
            _MaxSlope = 5.0f;
            _DiffSlope = _MaxSlope - _MinSlope;
            _MaxDensity = 0.8f;
            _MinDensity = 0.5f;
            _DiffDensity = _MaxDensity - _MinDensity;
        }

        private void RecalculateSize() {
            _Width = CCManager.main.width;
            _Height = CCManager.main.height;
            _WidthLimit = _Width - 1;
            _HeightLimit = _Height - 1;

            _Tiles = new CCGroupBufferTile[_Width][];

            for (int i = 0; i < _Width; i++) {
                _Tiles[i] = new CCGroupBufferTile[_Height];

                for (int j = 0; j < _Height; j++)
                    _Tiles[i][j] = new CCGroupBufferTile();
            }

            for (int i = 0; i < _Width; i++) {
                for (int j = 0; j < _Height; j++) {
                    _Tiles[i][j].right = GetTile(i + 1, j);
                    _Tiles[i][j].up = GetTile(i, j + 1);
                    _Tiles[i][j].left = GetTile(i - 1, j);
                    _Tiles[i][j].down = GetTile(i, j - 1);
                }
            }
        }

        public void RecalculateSpeedCost() {
            CCMainBuffer mainBuffer = CCManager.mainBuffer;

            for (int j = 0; j < _Height; j++) {
                CCGroupBufferTile[] tiles = _Tiles[j];
                
                for (int i = 0; i < _Width; i++) {
                    CCGroupBufferTile tile = _Tiles[i][j];
                    float[] costs = tile.costs;
                    float[] speeds = tile.speeds;

                    for (int k = 0; k < 4; k++) {
                        Vector2i dir = _Directions[k];
                        int targetX = i + dir.x;
                        int targetY = j + dir.y;

                        //If I was to move from this grid cell to the one in the given direction
                        // how bad would that be

                        //If we'd move off the grid, don't go that way (also if we'd walk in to a wall would go here)
                        if (!IsValid(targetX, targetY)) {
                            speeds[k] = int.MaxValue;
                            continue;
                        }

                        //Calculate by looking at the average velocity of the destination
                        // in the direction that we'd be travelling

                        CCMainBufferTile mainTile = mainBuffer.tiles[targetX][targetY];

                        var speedVecX = dir.x * mainTile.avgVelocity.x;
                        var speedVecY = dir.y * mainTile.avgVelocity.y;

                        //Get the only speed value as one will be zero
                        // this is like speedVecX != 0 ? : speedVecX : speedVecY
                        var flowSpeed = speedVecX + speedVecY;

                        var targetDensity = mainTile.density;
                        var targetDiscomfort = mainTile.discomfort;

                        if (targetDensity >= _MaxDensity) {
                            speeds[k] = flowSpeed;
                        }
                        else if (targetDensity <= _MinDensity) {
                            speeds[k] = _MaxSpeed;
                        }
                        else {
                            speeds[k] = _MaxSpeed - (targetDensity - _MinDensity) / _DiffDensity * (_MaxSpeed - flowSpeed);
                        }

                        //we're going to divide by speed later, so make sure it's not zero
                        speeds[k] = Mathf.Max(0.001f, speeds[k]);


                        //Work out the cost to move in to the destination cell
                        costs[k] = 2f / speeds[k]; //(speeds[k] * -1 + 8 + 1 * targetDiscomfort) / speeds[k];
                    }

                    /*float s0 = CalculateSpeed(i, j, 1, 0, Vector2.right);
                    float s1 = CalculateSpeed(i, j, 0, 1, Vector2.up);
                    float s2 = CalculateSpeed(i, j, -1, 0, Vector2.left);
                    float s3 = CalculateSpeed(i, j, 0, -1, Vector2.down);

                    tile.speeds[0] = s0;
                    tile.speeds[1] = s1;
                    tile.speeds[2] = s2;
                    tile.speeds[3] = s3;

                    tile.costs[0] = CalculateCost(i, j, 1, 0, s0);
                    tile.costs[1] = CalculateCost(i, j, 0, 1, s1);
                    tile.costs[2] = CalculateCost(i, j, -1, 0, s2);
                    tile.costs[3] = CalculateCost(i, j, 0, -1, s3);*/
                }
            }
        }

        public void RecalculatePotential(int x, int y) {
            CCMainBuffer mainBuffer = CCManager.mainBuffer;
            CCGroupBufferTile tile = GetTile(x, y);

            BeginPotential();

            tile.state = CCGroupBufferTileState.Known;
            tile.potential = 0;
            _Candidates.Add(tile);

            /*for (int i = -5; i < 5; i++ ) {
                for (int j = -5; j < 5; j++) {
                    CCGroupBufferTile t = GetTile(x + i, y + j);
                    t.potential = 0;
                    t.state = CCGroupBufferTileState.Known;
                    _Candidates.Add(t);
                }
            }*/

            while (_Candidates.Count > 0) {
                float min = 1000;
                foreach (CCGroupBufferTile t in _Candidates) {
                    if (t.potential < min) {
                        tile = t;
                        min = t.potential;
                    }
                }
                //tile = t;
                RemoveTile(tile);

                AddTile(tile.right, tile.potential, tile.costs[0]);
                AddTile(tile.up, tile.potential, tile.costs[1]);
                AddTile(tile.left, tile.potential, tile.costs[2]);
                AddTile(tile.down, tile.potential, tile.costs[3]);
            }
        }

        public void RecalculateFlow() {
            int i, j;

            //Generate an empty grid, set all places as Vector2.zero, which will stand for no good direction
            for (i = 0; i < _Width; i++) {
                CCGroupBufferTile[] tiles = _Tiles[i];

                for (j = 0; j < _Height; j++)
                    tiles[j].test = Vector2.zero;
            }

            //for each grid square
            for (i = 0; i < _Width; i++) {
                for (j = 0; j < _Height; j++) {
                    CCGroupBufferTile tile = _Tiles[i][j];

                    CalculateVelocity(tile);

                    /*//Obstacles have no flow value
                    if (tile.potential == float.MaxValue)
                        continue;

                    //Go through all neighbours and find the one with the lowest distance
                    Vector2i minDir = Vector2i.zero;
                    float minDist = float.MaxValue;

                    for (var d = 0; d < 4; d++) {
                        Vector2i dir = _Directions[d];


                        if (IsValid(i + dir.x, j + dir.y)) {
                            float dist = _Tiles[i + dir.x][j + dir.y].potential;

                            if (dist < minDist) {
                                minDir = dir;
                                minDist = dist;
                            }
                        }
                    }

                    //If we found a valid neighbour, point in its direction
                    if (minDir.x != 0 || minDir.y != 0) {
                        tile.test = new Vector2(minDir.x, minDir.y);
                    }*/
                }
            }
        }

        private void AddTile(CCGroupBufferTile tile, float potential, float cost) {
            float newCost = potential + cost;

            if (newCost < tile.potential || tile.state == CCGroupBufferTileState.Unknown) {
                tile.state = CCGroupBufferTileState.Candidate;
                tile.potential = newCost;
                _Candidates.Add(tile);
            }

            /*if (tile.state != CCGroupBufferTileState.Unknown)
                return;

            


            tile.state = CCGroupBufferTileState.Candidate;
            CalculatePotential(tile);
            _Candidates.Add(tile);*/
            

            /*int count = _Candidates.Count;

            for (int i = 0; i < count; i++) {
                if (_Candidates[i].potential > tile.potential)
                    _Candidates.Insert(i, tile);
            }*/
        }

        private void RemoveTile(CCGroupBufferTile tile) {
            tile.state = CCGroupBufferTileState.Known;
            _Candidates.Remove(tile);
        }

        private void BeginPotential() {
            for (int j = 0; j < _Height; j++) {
                for (int i = 0; i < _Width; i++) {
                    CCGroupBufferTile tile = GetTile(i, j);

                    tile.Reset();
                }
            }
        }

        private void CalculatePotential(CCGroupBufferTile tile) {
            //float right = tile.costs[0] + tile.right.potential;
            //float top = tile.costs[1] + tile.up.potential;
            //float left = tile.costs[2] + tile.left.potential;
            //float down = tile.costs[3] + tile.down.potential;
            //float x = 0;
            //float y = 0;
            //float dx = Mathf.Min(tile.right.potential, tile.left.potential);
            //float dy = Mathf.Min(tile.up.potential, tile.down.potential);
            float dx = Mathf.Min(tile.right.potential + tile.costs[0], tile.left.potential + tile.costs[2]);
            float dy = Mathf.Min(tile.up.potential + tile.costs[1], tile.down.potential + tile.costs[3]);
            float w = 1f;
            float delta = 2 * w * w - ((dx - dy) * (dx - dy));

            //tile.potential = Mathf.Min(dx + w, dy + w);
            
            if (delta >= 0)
                tile.potential = (dx + dy + Mathf.Sqrt(delta)) / 2;
            else
                tile.potential = Mathf.Min(dx + w, dy + w);
        }

        public Vector2 SampleVelocity(Vector3 position) {
            float fx = Mathf.Floor(position.x);
            float fy = Mathf.Floor(position.z);
            float dx = position.x - fx;
            float dy = position.z - fy;

            int bx = (int)fx;
            int by = (int)fy;
            int tx = bx + 1;
            int ty = by + 1;

            CCGroupBufferTile tile0 = GetTile(bx, by);
            CCGroupBufferTile tile1 = GetTile(tx, by);
            CCGroupBufferTile tile2 = GetTile(bx, ty);
            CCGroupBufferTile tile3 = GetTile(tx, ty);

            /*CalculateVelocity(tile0);
            CalculateVelocity(tile1);
            CalculateVelocity(tile2);
            CalculateVelocity(tile3);*/

            return Vector2.Lerp(Vector2.Lerp(tile0.test, tile1.test, dx), Vector2.Lerp(tile2.test, tile3.test, dx), dy);
        }

        private void CalculateVelocity(CCGroupBufferTile tile) {
            float rp = tile.right.potential;
            float lp = tile.left.potential;
            float up = tile.up.potential;
            float dp = tile.down.potential;
            float cp = tile.potential;
            float dx;
            float dy;

            if (rp < lp)
                dx = cp - rp;
            else
                dx = lp - cp;

            if (up < dp)
                dy = cp - up;
            else
                dy = dp - cp;

            tile.test = new Vector2(1/dx, 1/dy);
            tile.test.Normalize();
        }

        private float CalculateCost(int x, int y, int dx, int dy, float speed) {
            CCMainBufferTile tile = CCManager.mainBuffer.GetTile(x + dx, y + dy);

            return (speed + 1 + tile.discomfort) / speed;
        }

        private float CalculateSpeed(int x, int y, int dx, int dy, Vector2 direction) {
            CCMainBufferTile tile = CCManager.mainBuffer.GetTile(x + dx, y + dy);
            float speedTopology = CalculateSpeedTopology(x, y, dx, dy);
            float speedFlow = CalculateSpeedFlow(x, y, dx, dy, direction);
            float density = (tile.density - _MinDensity) / _DiffDensity;

            return speedTopology + density * (speedFlow - speedTopology);
        }

        private float CalculateSpeedTopology(int x, int y, int dx, int dy) {
            return 6f;
        }

        private float CalculateSpeedFlow(int x, int y, int dx, int dy, Vector2 direction) {
            CCMainBufferTile tile = CCManager.mainBuffer.GetTile(x + dx, y + dy);

            return Vector2.Dot(tile.avgVelocity, direction);
        }

        public int GetX(int x) {
            return Mathf.Clamp(x, 0, _WidthLimit);
        }

        public int GetY(int y) {
            return Mathf.Clamp(y, 0, _HeightLimit);
        }

        public bool IsValid(int x, int y) {
            return 0 <= x && x < _Width && 0 <= y && y < _Height;
        }

        public CCGroupBufferTile GetTile(int x, int y) {
            return _Tiles[Mathf.Clamp(x, 0, _WidthLimit)][Mathf.Clamp(y, 0, _HeightLimit)];
        }
    }
}
