﻿using UnityEngine;
using System.Collections.Generic;

namespace Warcraft.CC {
    public class CCDensityBuffer {

        private Vector2[][] _VelocityBuffer;
        private float[][] _DensityBuffer;
        private int _Width;
        private int _Height;

        public CCDensityBuffer() {
            _Width = CCManager.main.width;
            _Height = CCManager.main.height;

            _DensityBuffer = new float[_Width][];
            _VelocityBuffer = new Vector2[_Width][];

            for (int i = 0; i < _Width; i++) {
                _DensityBuffer[i] = new float[_Height];
                _VelocityBuffer[i] = new Vector2[_Height];
            }

            Begin();
        }

        public void Set(List<CCAgent> agents) {
            int count = agents.Count;

            for (int i = 0; i < count; i++) 
                Set(agents[i]);
        }

        public void Set(CCAgent agent) {
            Vector2 position = new Vector2(agent.transform.position.x, agent.transform.position.z);

            Set(position, agent.velocity);
        }

        public void Set(Vector2 position, Vector2 velocity) {
            float x = position.x - 0.5f;
            float y = position.y - 0.5f;
            float fx = Mathf.Floor(x);
            float fy = Mathf.Floor(y);
            float dx = x - fx;
            float dy = y - fy;

            int bx = Mathf.Clamp((int)fx, 0, _Width - 1);
            int by = Mathf.Clamp((int)fy, 0, _Height - 1);
            int tx = Mathf.Clamp(bx + 1, 0, _Width - 1);
            int ty = Mathf.Clamp(by + 1, 0, _Height - 1);

            float d1 = Mathf.Min(1f - dx, 1 - dy);
            float d2 = Mathf.Min(dx, 1 - dy);
            float d3 = Mathf.Min(1f - dx, dy);
            float d4 = Mathf.Min(dx, dy);

            _DensityBuffer[bx][by] += d1;
            _DensityBuffer[tx][by] += d2;
            _DensityBuffer[bx][ty] += d3;
            _DensityBuffer[tx][ty] += d4;

            _VelocityBuffer[bx][by] += velocity * d1;
            _VelocityBuffer[tx][by] += velocity * d2;
            _VelocityBuffer[bx][ty] += velocity * d3;
            _VelocityBuffer[tx][ty] += velocity * d4;
        }

        public float SampleDensity(Vector2 position) {
            float fx = Mathf.Floor(position.x);
            float fy = Mathf.Floor(position.y);
            float dx = position.x - fx;
            float dy = position.y - fy;

            int bx = Mathf.Clamp((int)fx, 0, _Width - 1);
            int by = Mathf.Clamp((int)fy, 0, _Height - 1);
            int tx = Mathf.Clamp(bx + 1, 0, _Width - 1);
            int ty = Mathf.Clamp(by + 1, 0, _Height - 1);

            return Mathf.Lerp(Mathf.Lerp(_DensityBuffer[bx][by], _DensityBuffer[tx][by], dx), Mathf.Lerp(_DensityBuffer[bx][ty], _DensityBuffer[tx][ty], dx), dy);
        }

        public float GetDensity(Vector2 position) {
            return _DensityBuffer[(int)position.x][(int)position.y];
        }

        public Vector2 GetVelocity(Vector2 position) {
            return _VelocityBuffer[(int)position.x][(int)position.y];
        }

        public void Begin() {
            for (int j = 0; j < _Height; j++) {
                for (int i = 0; i < _Width; i++) {
                    _VelocityBuffer[i][j] = Vector3.zero;
                    _DensityBuffer[i][j] = 0f;
                }
            }
        }

        public void End() {
            for (int j = 0; j < _Height; j++) {
                for (int i = 0; i < _Width; i++) {
                    _VelocityBuffer[i][j] /= _DensityBuffer[i][j];
                }
            }
        }
    }
}
