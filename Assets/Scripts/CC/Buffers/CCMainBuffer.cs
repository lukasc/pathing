﻿using UnityEngine;
using System.Collections.Generic;

namespace Warcraft.CC {
    public class CCMainBufferTile {

        public Vector2 avgVelocity;
        public float density;
        public float height;
        public float discomfort;
        public Vector2[] slopes;

        public CCMainBufferTile() {
            slopes = new Vector2[4];
        } 
    }

    public class CCMainBuffer {

        private CCMainBufferTile[][] _Tiles;
        private int _Width;
        private int _Height;
        private int _WidthLimit;
        private int _HeightLimit;

        public CCMainBufferTile[][] tiles { get { return _Tiles; } }

        public CCMainBuffer() {
            RecalculateSize();
        }

        private void RecalculateSize() {
            _Width = CCManager.main.width;
            _Height = CCManager.main.height;
            _WidthLimit = _Width - 1;
            _HeightLimit = _Height - 1;

            _Tiles = new CCMainBufferTile[_Width][];

            for (int i = 0; i < _Width; i++) {
                _Tiles[i] = new CCMainBufferTile[_Height];

                for (int j = 0; j < _Height; j++)
                    _Tiles[i][j] = new CCMainBufferTile();
            }
        }

        public void RecalculateSlope() {
            for (int j = 0; j < _Height; j++) {
                for (int i = 0; i < _Width; i++) {
                    CCMainBufferTile tile = GetTile(i, j);
                    CCMainBufferTile tile0 = GetTile(i + 1, j);
                    CCMainBufferTile tile1 = GetTile(i, j + 1);
                    CCMainBufferTile tile2 = GetTile(i - 1, j);
                    CCMainBufferTile tile3 = GetTile(i, j - 1);
                    float height = tile0.height;

                    tile.slopes[0] = Vector2.zero;
                    tile.slopes[1] = Vector2.zero;
                    tile.slopes[2] = Vector2.zero;
                    tile.slopes[3] = Vector2.zero;
                }
            }
        }

        public void RecalculateDensity(List<CCAgent> agents) {
            BeginDensity();
            SetDensity(agents);
            EndDensity();
        }

        private void BeginDensity() {
            for (int j = 0; j < _Height; j++) {
                for (int i = 0; i < _Width; i++) {
                    CCMainBufferTile tile = GetTile(i, j);

                    tile.density = 0;
                    tile.avgVelocity = Vector2.zero;
                }
            }
        }

        private void EndDensity() {
            for (int j = 0; j < _Height; j++) {
                for (int i = 0; i < _Width; i++) {
                    CCMainBufferTile tile = GetTile(i, j);

                    if (tile.density > 0)
                        tile.avgVelocity /= tile.density;
                }
            }
        }

        private void SetDensity(List<CCAgent> agents) {
            int count = agents.Count;

            for (int i = 0; i < count; i++)
                SetDensity(agents[i]);
        }

        private void SetDensity(CCAgent agent) {
            Vector2 position = new Vector2(agent.transform.position.x, agent.transform.position.z);

            SetDensity(position, agent.velocity);
        }

        private void SetDensity(Vector2 position, Vector2 velocity) {
            float x = position.x - 0.5f;
            float y = position.y - 0.5f;
            float fx = Mathf.Floor(x);
            float fy = Mathf.Floor(y);
            float dx = x - fx;
            float dy = y - fy;

            int bx = (int)fx;
            int by = (int)fy;
            int tx = bx + 1;
            int ty = by + 1;

            float d1 = Mathf.Min(1f - dx, 1 - dy);
            float d2 = Mathf.Min(dx, 1 - dy);
            float d3 = Mathf.Min(1f - dx, dy);
            float d4 = Mathf.Min(dx, dy);

            CCMainBufferTile t1 = GetTile(bx, by);
            CCMainBufferTile t2 = GetTile(tx, by);
            CCMainBufferTile t3 = GetTile(bx, ty);
            CCMainBufferTile t4 = GetTile(tx, ty);

            t1.density += d1;
            t2.density += d2;
            t3.density += d3;
            t4.density += d4;
            
            t1.avgVelocity += velocity * d1;
            t2.avgVelocity += velocity * d2;
            t3.avgVelocity += velocity * d3;
            t4.avgVelocity += velocity * d4;
        }

        public CCMainBufferTile GetTile(int x, int y) {
            return _Tiles[Mathf.Clamp(x, 0, _WidthLimit)][Mathf.Clamp(y, 0, _HeightLimit)];
        }
    }
}