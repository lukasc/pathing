﻿using UnityEngine;
using System.Collections.Generic;

namespace Warcraft.CC {
    public class CCAgent : MonoBehaviour {

        private static Vector2 _Sampler0 = new Vector2(1, 0);
        private static Vector2 _Sampler1 = new Vector2(0.707f, 0.707f);
        private static Vector2 _Sampler2 = new Vector2(0.707f, -0.707f);
        private static Vector2 _Sampler3 = new Vector2(0, -1);
        private static Vector2 _Sampler4 = new Vector2(0, 1);

        [SerializeField] private Transform _Target;
        [SerializeField] private float _Radius;
        [SerializeField] private float _Speed;
        [SerializeField] private float _Acceleration;

        private Vector2 _Velocity;
        private Vector2 _Direction;
        private CCAgentGroup _Group;

        public Vector2 velocity { get { return _Velocity; } set { _Velocity = value; } }
        public Vector2 direction { get { return _Direction; } }

        public Vector2 Rotate(Vector2 source, Vector2 direction) {
            return new Vector2(source.x * direction.x - source.y * direction.y, source.x * direction.y + source.y * direction.x);
        }

        public void Seperate() {
            List<CCAgent> agents = CCManager.main.agents;
            int count = agents.Count;

            for (int i = 0; i < count; i++) {
                CCAgent agent = agents[i];
                Vector3 direction = transform.position - agent.transform.position;
                float distance = _Radius + agent._Radius - direction.magnitude;

                if (agent == this)
                    continue;

                if (distance > 0) {
                    transform.position += direction.normalized * distance;
                }
            }
        }

        public void Avoidance() {
            CCDensityBuffer densityBuffer = CCManager.densityBuffer;
            Vector2 avoidance = Vector2.zero;
            Vector2 position = new Vector2(transform.position.x, transform.position.z);
            int radius = 4;
            int radius2 = radius * radius;
            int count = 0;

            for (int j = -radius; j < radius; j++) {
                for (int i = -radius; i < radius; i++) {
                    float distance = i * i + j * j;

                    if (distance > radius2 || distance < 1.5)
                        continue;

                    Vector2 direction = new Vector2(i, j);
                    if (Vector2.Dot(direction, _Velocity) < 0)
                        continue;
                    float density = densityBuffer.SampleDensity(position + direction);

                    if (distance > 0) {
                        avoidance += direction / distance * density;
                        count++;
                    }
                }
            }

            if (count > 0)
                _Velocity -= avoidance / count * _Speed * 5;
        }

        public void CalculateDirection() {
            CCDensityBuffer densityBuffer = CCManager.densityBuffer;
            Vector2 s0 = Rotate(_Sampler0, _Velocity);
            Vector2 s1 = Rotate(_Sampler1, _Velocity);
            Vector2 s2 = Rotate(_Sampler2, _Velocity);
            Vector2 s3 = Rotate(_Sampler3, _Velocity);
            Vector2 s4 = Rotate(_Sampler4, _Velocity);
            Vector2 position = new Vector2(transform.position.x, transform.position.z);

            Vector2 direction = Vector2.zero;

            direction -= s0 * densityBuffer.SampleDensity(s0 + position);
            direction -= s1 * densityBuffer.SampleDensity(s1 + position);
            direction -= s2 * densityBuffer.SampleDensity(s2 + position);
            direction -= s3 * densityBuffer.SampleDensity(s3 + position);
            direction -= s4 * densityBuffer.SampleDensity(s4 + position);

            Debug.DrawLine(transform.position, transform.position + new Vector3(s0.x, 0, s0.y));
            Debug.DrawLine(transform.position, transform.position + new Vector3(s1.x, 0, s1.y));
            Debug.DrawLine(transform.position, transform.position + new Vector3(s2.x, 0, s2.y));
            Debug.DrawLine(transform.position, transform.position + new Vector3(s3.x, 0, s3.y));
            Debug.DrawLine(transform.position, transform.position + new Vector3(s4.x, 0, s4.y));

            direction /= 3f;
            
            _Velocity += direction * _Speed;
        }

        public void Step() {
            Vector3 direction = (_Target.transform.position - transform.position).normalized;
            Vector2 move = new Vector2(direction.x, direction.z) * _Acceleration;

            _Velocity += move;

            float speed = _Velocity.magnitude;

            if (speed > _Speed) {
                _Velocity.Normalize();
                _Velocity *= _Speed;
            }

            Debug.DrawLine(transform.position, transform.position + new Vector3(_Velocity.x, 0, _Velocity.y), Color.red);
            //Debug.Log(_Direction);
            transform.Translate(_Velocity.x * Time.deltaTime, 0, _Velocity.y * Time.deltaTime);
        }

        public void AddVelocity(Vector2 velocity) {
            _Velocity += velocity;

            float speed = _Velocity.magnitude;

            if (speed > _Speed) {
                _Velocity.Normalize();
                _Velocity *= _Speed;
            }

            transform.Translate(_Velocity.x * Time.deltaTime, 0, _Velocity.y * Time.deltaTime);
        }

        internal void OnEnable() {
            CCManager.main.AddAgent(this);
        }

        internal void OnDisable() {
            CCManager.main.RemoveAgent(this);
        }

        internal void Awake() {
            if (_Target != null) {
                _Group = CCManager.main.SetDestination(this, new Vector2(_Target.transform.position.x, _Target.transform.position.z), 1f);
            }
            //CCManager.main.AddAgent(this);
            //_Direction = new Vector2(1, 0);
        }

        internal void Update() {
            if (_Group != null)
                _Group.goal = new Vector2(_Target.transform.position.x, _Target.transform.position.z);
            //CalculateDirection();
            //Avoidance();
            //Seperate();
            //Step();
        }
    }
}
