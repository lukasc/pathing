﻿using UnityEngine;

namespace Warcraft.CC {
    public class CCDebugDensity : MonoBehaviour {

        private Texture2D _Texture;

        public void Step() {
            CCAgentGroup group = CCManager.main.groups[1];
            CCGroupBuffer buffer = group.buffer;

            for (int j = 0; j < CCManager.main.height; j++) {
                for (int i = 0; i < CCManager.main.width; i++) {
                    Vector2 gradient = buffer.GetTile(i, j).test;
                    Vector3 centre = new Vector3(i + 0.5f, 0.5f, j + 0.5f);
                    Vector3 direction = new Vector3(gradient.x, 0.5f, gradient.y);

                    Debug.DrawLine(centre, centre + direction/2f, Color.red);
                    Debug.DrawLine(centre, centre + direction/3f, Color.green);
                    //_Texture.SetPixel(i, j, Color.blue * (CCManager.densityBuffer.GetDensity(new Vector2(i, j)) - 0f));
                }
            }

            _Texture.SetPixels(0, 0, _Texture.width, _Texture.height, CCManager.main.groups[1].CalculateColors());
            _Texture.Apply();
        }



        /*internal void OnDrawGizmos() {
            CCAgentGroup group = CCManager.main.groups[1];
            CCGroupBuffer buffer = group.buffer;

            for (int j = 0; j < CCManager.main.height; j++) {
                //GUILayout.BeginHorizontal();
                for (int i = 0; i < CCManager.main.width; i++) {
                    float potential = buffer.GetTile(i, j).potential;


                    UnityEditor.Handles.Label(new Vector3(i + 0.5f, 0, j + 0.5f), potential.ToString());
                }
                //GUILayout.EndHorizontal();
            }
        }*/



        internal void Awake() {
            _Texture = new Texture2D(CCManager.main.width, CCManager.main.height);
            _Texture.filterMode = FilterMode.Point;

            CCManager.main.groups[0].CalculateColors();

            GetComponent<Renderer>().material.mainTexture = _Texture;
        }

        internal void Update() {
            //Step();
        }
    }
}
