﻿using UnityEngine;
using System.Collections.Generic;

namespace Warcraft.CC {
    public class CCManager : MonoBehaviour {

        private static CCManager _Main;
        private static CCDensityBuffer _DensityBuffer;
        private static CCMainBuffer _MainBuffer;

        [SerializeField] private int _Width;
        [SerializeField] private int _Height;
        [SerializeField] private float _TimeStep;

        private List<CCAgent> _Agents;
        private List<CCAgentGroup> _Groups;
        
        private float _Time;

        public static CCManager main { get { return _Main; } }
        public static CCDensityBuffer densityBuffer { get { return _DensityBuffer; } }
        public static CCMainBuffer mainBuffer { get { return _MainBuffer; } }

        public List<CCAgent> agents { get { return _Agents; } }
        public List<CCAgentGroup> groups { get { return _Groups; } }
        public int width { get { return _Width; } }
        public int height { get { return _Height; } }

        public void AddAgent(CCAgent agent) {
            _Agents.Add(agent);
        }

        public void RemoveAgent(CCAgent agent) {
            _Agents.Remove(agent);
        }

        public void AddGroup(CCAgentGroup group) {
            _Groups.Add(group);
        }

        public void Step() {
            _MainBuffer.RecalculateDensity(_Agents);

            int count = _Groups.Count;

            for (int i = 0; i < count; i++)
                _Groups[i].Recalculate();
        }

        public CCAgentGroup SetDestination(CCAgent agent, Vector2 goal, float radius) {
            int count = _Groups.Count;

            for (int i = 0; i < count; i++) {
                CCAgentGroup group = _Groups[i];

                if (group.Try(agent, goal, radius))
                    return group;
            }

            return null;
        }

        internal void Awake() {
            _Main = this;
            _Agents = new List<CCAgent>(32);
            _Groups = new List<CCAgentGroup>(2);
            _MainBuffer = new CCMainBuffer();

            _Groups.Add(new CCAgentGroup());
            _Groups.Add(new CCAgentGroup());

            _MainBuffer.RecalculateSlope();
        }

        internal void Update() {
            _Time += Time.deltaTime;

            if (_Time >= _TimeStep)
                Step();
        }
    }
}
