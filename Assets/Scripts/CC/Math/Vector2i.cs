﻿using UnityEngine;

namespace Warcraft.CC {
    public struct Vector2i {

        public static Vector2i zero = new Vector2i(0, 0);
        public static Vector2i right = new Vector2i(1, 0);
        public static Vector2i up = new Vector2i(0, 1);
        public static Vector2i left = new Vector2i(-1, 0);
        public static Vector2i down = new Vector2i(0, -1);

        public int x;
        public int y;

        public Vector2i(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
