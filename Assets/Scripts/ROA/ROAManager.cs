﻿using UnityEngine;
using System.Collections.Generic;

namespace Warcraft {
    public class ROAManager : MonoBehaviour {

        private static ROAManager _Main;

        private const float _SeperationWeight = 2.2f;
        private const float _AlignmentWeight = 0.3f;
        private const float _CohesionWeight = 0.05f;

        private static List<ROAAgent> _Agents;
        private static List<QuadTreeAgent> _QuadAgents;

        private QuadTree _QuadTree;

        public static ROAManager main { get { return _Main; } }
        public static List<ROAAgent> agents { get { return _Agents; } }

        internal void Awake() {
            _Main = this;
            _Agents = new List<ROAAgent>();
            _QuadTree = new QuadTree(0, 0, 64, 64, 2);
        }

        internal void Update() {
	        int count = _Agents.Count;

            for (var i = 0; i < count; i++)
                _Agents[i].avoidance = ROAAvoidance.None;

	        for (var i = 0; i < count; i++) {
		        ROAAgent agent = _Agents[i];
                Vector2 impulse = Vector2.zero;

                if (agent.state != ROAState.Moving)
                    continue;

                _QuadAgents = _QuadTree.GetAgents(agent.position, 2f);

                //if (agent.state == ROAState.Moving) {
                    Vector2 avoid = GetAvoid2(agent);

                    impulse += GetSeek(agent, agent.destination);
                    impulse += GetAlignment(agent) * _AlignmentWeight;
                    impulse += GetCohesion(agent) * _CohesionWeight;
                    impulse += avoid;
                    impulse += GetSeparation(agent) * _SeperationWeight;
                    Arrival(agent);

                    Debug.DrawLine(agent.position, agent.position + avoid, Color.red);
                //}


                
                
                

		        /*Vector2 seek = GetSeek(agent, agent.destination);
                Vector2 seperation = GetSeparation(agent);
                Vector2 alignment = GetAlignment(agent);
                Vector2 cohesion = GetCohesion(agent);
                Vector2 avoid = GetAvoid2(agent);

                agent.impulse = seek + seperation * _SeperationWeight + alignment * _AlignmentWeight + cohesion * _CohesionWeight + avoid;*/

               // Debug.DrawLine(agent.position, agent.position + agent.velocity);
               
                

		        /*float lengthSquared = agent.impulse.sqrMagnitude;

		        if (lengthSquared > agent.maxForceSquared)
                    agent.impulse *= agent.maxForce / Mathf.Sqrt(lengthSquared);*/

                agent.ApplyImpulse(impulse);
                Seperate(agent);
                
                _QuadTree.Recalculate(agent, agent.position);
                //Debug.Log(_QuadAgents.Count);
                //Debug.DrawLine(agent.position, agent.position + agent.impulse, Color.green);
	        }

            /*for (var i = 0; i < count; i++) {
                ROAAgent agent = _Agents[i];

                agent.ApplyImpulse();
                Seperate(agent);
                Arrival(agent);
	        }*/
        }

        public void AddAgent(ROAAgent agent) {
            _Agents.Add(agent);
            _QuadTree.Recalculate(agent, agent.position);
        }

        private void Arrival(ROAAgent agent) {
            float distance = Vector2.Distance(agent.position, agent.destination);

            if (distance <= agent.stopingDistance)
                agent.Stop();
        }

        private Vector2 GetSeek(ROAAgent agent, Vector2 destination) {
            Vector2 position = agent.position;

	        if (position == destination) 
		        return Vector2.zero;

	        Vector2 desired = destination - position;

            desired *= agent.maxSpeed / desired.magnitude;

	        Vector2 velocityChange = desired - agent.velocity;

            velocityChange *= agent.maxForce / agent.maxSpeed;

	        return velocityChange;
        }

        public Vector2 GetSeparation(ROAAgent agent) {
	        Vector2 totalForce = Vector2.zero;
	        int neighboursCount = 0;
            int count = _QuadAgents.Count;

	        for (var i = 0; i < count; i++) {
                ROAAgent a = _QuadAgents[i] as ROAAgent;

		        if (a != agent && a.group == agent.group) {
                    Vector2 pushForce = agent.position - a.position;
			        float distance = pushForce.magnitude;

			        if (distance < agent.separation && distance > 0) {
				        float r = (agent.radius + a.radius);

				        totalForce += pushForce * (1f - ((distance - r) / (agent.separation - r)));

				        neighboursCount++;
			        }
		        }
	        }

	        if (neighboursCount == 0)
		        return Vector2.zero;

            totalForce *= agent.maxForce / neighboursCount;

	        return totalForce;
        }

        private Vector2 GetCohesion(ROAAgent agent) {
	        Vector2 centerOfMass = Vector2.zero;
	        int neighboursCount = 0;
            int count = _QuadAgents.Count;

	        for (var i = 0; i < count; i++) {
                ROAAgent a = _QuadAgents[i] as ROAAgent;

		        if (agent != a && agent.SameGroup(a)) {
			        float distance = Vector2.Distance(agent.transform.position, a.transform.position);

			        if (distance < agent.cohesion) {
				        centerOfMass += a.position;

				        neighboursCount++;
			        }
		        }
	        }

	        if (neighboursCount == 0)
		        return Vector2.zero;

	        centerOfMass /= neighboursCount;

	        return GetSeek(agent, centerOfMass);
        }

        private Vector2 GetAlignment(ROAAgent agent) {
	        Vector2 averageHeading = Vector2.zero;
	        int neighboursCount = 0;
            int count = _QuadAgents.Count;

	        for (var i = 0; i < count; i++) {
                ROAAgent a = _QuadAgents[i] as ROAAgent;
		        float distance = Vector2.Distance(agent.transform.position, a.transform.position);

		        if (distance < agent.cohesion && a.velocity.magnitude > 0 && agent.SameGroup(a)) {
			        Vector2 head = a.velocity.normalized;

			        averageHeading += head;
			        neighboursCount++;
		        }
	        }

	        if (neighboursCount == 0)
		        return averageHeading;

	        averageHeading /= neighboursCount;

	        return GetSteerTowards(agent, averageHeading);
        }

        public Vector2 GetAvoid2(ROAAgent agent) {
            ROAAgent a = null;
            int count = _QuadAgents.Count;
            float minDistance = 2f;

            for (var i = 0; i < count; i++) {
                ROAAgent t = _QuadAgents[i] as ROAAgent;
                Vector2 direction = t.position - agent.position;
                float distance = direction.magnitude;

                if (distance < minDistance && agent != t && Vector2.Dot(agent.velocity, direction) > 0) {
                    minDistance = distance;
                    a = t;
                }
            }

            if (a == null)
                return Vector2.zero;

            Vector2 combinedVelocity = agent.velocity + a.velocity;
            float ourVelocityLengthSquared = agent.velocity.sqrMagnitude;
            float combinedVelocityLengthSquared = combinedVelocity.sqrMagnitude;
            bool haveAvoidance = a.avoidance != ROAAvoidance.None;

            if (combinedVelocityLengthSquared > ourVelocityLengthSquared && !haveAvoidance)
                return Vector2.zero;

            Vector2 vectorInOtherDirection = a.position - agent.position;
            Vector2 avoid = Vector2.zero;

            if (haveAvoidance) {
                agent.avoidance = a.avoidance;
            }
            else {
                float dot = agent.velocity.x * -vectorInOtherDirection.y + agent.velocity.y * vectorInOtherDirection.x;

                if (dot > 0)
                    agent.avoidance = ROAAvoidance.Left;
                    //avoid = new Vector2(-vectorInOtherDirection.y, vectorInOtherDirection.x);
                else
                    agent.avoidance = ROAAvoidance.Right;
                    //avoid = new Vector2(vectorInOtherDirection.y, -vectorInOtherDirection.x);
            }

            avoid = agent.avoidance == ROAAvoidance.Left ? new Vector2(-vectorInOtherDirection.y, vectorInOtherDirection.x) : new Vector2(vectorInOtherDirection.y, -vectorInOtherDirection.x);
            avoid.Normalize();
            avoid *= agent.radius + a.radius;

            return GetSteerTowards(agent, avoid) / minDistance * 5f;
        }

        public void Seperate(ROAAgent agent) {
            int count = _QuadAgents.Count;

            for (int i = 0; i < count; i++) {
                ROAAgent a = _QuadAgents[i] as ROAAgent;
                Vector3 direction = agent.transform.position - a.transform.position;
                float distance = agent.radius + a.radius - direction.magnitude;

                if (agent == a)
                    continue;

                if (distance > 0) {
                    agent.transform.position += direction.normalized * distance;
                }
            }
        }

        /*public Vector2 GetAvoid(ROAAgent agent) {
            if (agent.velocity.sqrMagnitude <= agent.radius)
                return Vector2.zero;

            Vector2 start = agent.position;
            Vector2 end = start + agent.velocity;
            Vector2 offset = agent.velocity;
            Rigidbody2D rigidbody = null;
            float temp = offset.x;
            float minFraction = 2f;

            offset.x = offset.y;
            offset.y = -temp;
            offset *= agent.radius;

            RaycastHit2D hit;
            agent.gameObject.layer = 1;
            hit = Physics2D.Linecast(start, end, 0);

            if (hit.collider != null) {
                float fraction = hit.fraction;

                if (fraction < minFraction) {
                    minFraction = fraction;
                    rigidbody = hit.rigidbody;
                }
            }

            hit = Physics2D.Linecast(start, end + offset, 0);

            if (hit.collider != null) {
                float fraction = hit.fraction;

                if (fraction < minFraction) {
                    minFraction = fraction;
                    rigidbody = hit.rigidbody;
                }
            }

            hit = Physics2D.Linecast(start, end - offset, 0);

            if (hit.collider != null) {
                float fraction = hit.fraction;

                if (fraction < minFraction) {
                    minFraction = fraction;
                    rigidbody = hit.rigidbody;
                }
            }
            agent.gameObject.layer = 0;
            if (rigidbody == null)
                return Vector2.zero;

            ROAAgent a = rigidbody.GetComponent<ROAAgent>();
            Vector2 combinedVelocity = agent.velocity + a.velocity;
            float ourVelocityLengthSquared = agent.velocity.sqrMagnitude;
            float combinedVelocityLengthSquared = combinedVelocity.sqrMagnitude;
            bool haveAvoidance = a.avoidance != Vector2.zero;

            if (combinedVelocityLengthSquared > ourVelocityLengthSquared && !haveAvoidance)
                return Vector2.zero;

            Vector2 vectorInOtherDirection = a.position - agent.position;
            Vector2 avoid = Vector2.zero;

            if (haveAvoidance) {
                avoid = a.avoidance;
            }
            else {
                float dot = agent.velocity.x * -vectorInOtherDirection.y + agent.velocity.y * vectorInOtherDirection.x;

                if (dot > 0) 
                    avoid = new Vector2(-vectorInOtherDirection.y, vectorInOtherDirection.x);
                else
                    avoid = new Vector2(vectorInOtherDirection.y, -vectorInOtherDirection.x);
            }

            agent.avoidance = avoid;
            avoid.Normalize();
            avoid *= agent.radius + a.radius;

            return GetSteerTowards(agent, avoid) / minFraction;
        }*/

        private Vector2 GetSteerTowards(ROAAgent agent, Vector2 desiredDirection) {
	        Vector2 desiredVelocity = desiredDirection * agent.maxSpeed;
	        Vector2 velocityChange = desiredVelocity - agent.velocity;

            velocityChange *= agent.maxForce / agent.maxSpeed;

	        return velocityChange;
        }
    }
}


