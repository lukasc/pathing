﻿using UnityEngine;

namespace Warcraft {
    public enum ROAAvoidance {
        None,
        Left,
        Right,
    }

    public enum ROAState {
        Idle,
        Waiting,
        Moving,
    }

    public class ROAAgent :  MonoBehaviour, QuadTreeAgent {

        [SerializeField] private float _StopingDistance = 0f;
        [SerializeField] private float _MaxForce = 50f;
        [SerializeField] private float _MaxSpeed = 4f;
        [SerializeField] private float _Radius = 0.23f;
        [SerializeField] private Transform _Target;
        [SerializeField] private int _Group;

        private ROAState _State;
        private Rigidbody2D _RigidBody;
        private Collider2D _Collider;
        private Vector2 _Destination;
        private Vector2 _Velocity;
        private ROAAvoidance _Avoidance;
        private Vector2 _Impulse;
        private QuadTreeNode _QuadTreeNode;
        
        private float _Separation;
        private float _Cohesion;
        private float _MaxForceSquared;
        private float _MaxSpeedSquared;

        public ROAState state { get { return _State; } }
        public Rigidbody2D rigidBody { get { return _RigidBody; } }
        public Collider2D collider { get { return _Collider; } }
        public Vector2 impulse { get { return _Impulse; } set { _Impulse = value; } }
        public Vector2 destination { get { return _Destination; } }
        public Vector2 velocity { get { return _Velocity; } }
        public ROAAvoidance avoidance { get { return _Avoidance; } set { _Avoidance = value; } }
        public QuadTreeNode quadTreeNode { get { return _QuadTreeNode; } set { _QuadTreeNode = value; } }
        public float stopingDistance { get { return _StopingDistance; } }
        public int group { get { return _Group; } }
        public float maxForce { get { return _MaxForce; } }
        public float maxSpeed { get { return _MaxSpeed; } }
        public float radius { get { return _Radius; } }
        public float separation { get { return _Separation; } }
        public float cohesion { get { return _Cohesion; } }
        public float maxForceSquared { get { return _MaxForceSquared; } }
        public float maxSpeedSquared { get { return _MaxSpeedSquared; } }

        public Vector2 position {
            get {
                Vector3 pos = transform.position;

                return new Vector2(pos.x, pos.y);
            }
        }

        public bool SameGroup(ROAAgent agent) {
            return agent.group == _Group;
        }

        public bool SetDestination(Vector2 destination) {
            if (destination == _Destination)
                return false;

            _Destination = destination;
            _State = ROAState.Moving;

            return true;
        }

        public void Stop() {
            _State = ROAState.Idle;
            _Velocity = Vector2.zero;
        }

        public void Resume() {
            _State = ROAState.Moving;
        }

        public void ApplyImpulse(Vector2 impulse) {
            float lengthSquared = impulse.sqrMagnitude;

            if (lengthSquared > maxForceSquared)
                impulse *= maxForce / Mathf.Sqrt(lengthSquared);
            
            impulse *= Time.deltaTime;
            _Velocity += impulse;

            float magnitude = _Velocity.magnitude;

            if (magnitude > _MaxSpeed) {
                _Velocity *= _MaxSpeed / magnitude;
            }

            transform.Translate(_Velocity.x, _Velocity.y, 0);
            //_RigidBody.AddForce(_Impulse);
        }

        internal void Awake() {
            
            _Separation = _Radius * 4f;
            _Cohesion = _Radius * 10f;
            _MaxForceSquared = _MaxForce * _MaxForce;
            _MaxSpeedSquared = _MaxSpeed * _MaxSpeed;

            /*CircleCollider2D circle = gameObject.AddComponent<CircleCollider2D>();
            circle.radius = _Radius;

            _Collider = circle;

            Rigidbody2D rigidBody = gameObject.AddComponent<Rigidbody2D>();
            rigidBody.drag = 0.2f;

            _RigidBody = rigidBody;*/

            ROAManager.main.AddAgent(this);

        }

        internal void Update() {
            if (_Target != null)
                SetDestination(_Target.transform.position);
        }
    }
}
