﻿using UnityEngine;

namespace Warcraft {
    public class TestNavmesh : MonoBehaviour {

        [SerializeField]
        private Transform _Target;

        private NavMeshAgent _NavmeshAgent;

        internal void Awake() {
            _NavmeshAgent = GetComponent<NavMeshAgent>();
        }

        internal void Update() {
            _NavmeshAgent.SetDestination(_Target.position);
        }
    }
}
