﻿using System.Collections.Generic;
using UnityEngine;

namespace Warcraft {
    public interface QuadTreeAgent {

        QuadTreeNode quadTreeNode { set; get; }
    }

    public class QuadTreeNode {

        private static Vector2[] _Quads = {
                                                new Vector2(0, 0),
                                                new Vector2(1, 0),
                                                new Vector2(1, 1),
                                                new Vector2(0, 1),
                                                };

        private QuadTreeNode _Parent;
        private QuadTreeNode[] _Childs;
        private Rect _Rect;
        private List<QuadTreeAgent> _Agents;

        public int agentsCount { get { return _Agents.Count; } }
             
        public QuadTreeNode(QuadTreeNode parent, Rect rect) {
            _Parent = parent;
            _Rect = rect;

            Recalculate();
        }

        private void Recalculate() {
            QuadTree quadTree = QuadTree.current;

            if (_Rect.width <= quadTree.nodeSize) {
                _Agents = new List<QuadTreeAgent>();
                quadTree.SetGameQuadNode(this, _Rect.position.x, _Rect.position.y);

                return;
            }

            _Childs = new QuadTreeNode[4];

            Vector2 size = _Rect.size / 2f;

            for (int i = 0; i < 4; i++) {
                Vector2 quad = _Quads[i];
                float left = _Rect.position.x + quad.x * size.x;
                float top = _Rect.position.y + quad.y * size.y;
                Rect rect = new Rect(left, top, size.x, size.y);
                
                _Childs[i] = new QuadTreeNode(this, rect);
            }
        }

        public void CalculateAgents(Rect rect) {
            if (_Rect.Overlaps(rect)) {
                if (_Childs == null) {
                    QuadTree.current.result.AddRange(_Agents);
                }
                else {
                    foreach (QuadTreeNode node in _Childs)
                        node.CalculateAgents(rect);
                }
            }
        }

        public void Draw(Color color, float time) {
            Debug.DrawLine(new Vector3(_Rect.xMin, 0, _Rect.yMin), new Vector3(_Rect.xMax, 0, _Rect.yMin), color, time);
            Debug.DrawLine(new Vector3(_Rect.xMin, 0, _Rect.yMax), new Vector3(_Rect.xMax, 0, _Rect.yMax), color, time);
            Debug.DrawLine(new Vector3(_Rect.xMin, 0, _Rect.yMin), new Vector3(_Rect.xMin, 0, _Rect.yMax), color, time);
            Debug.DrawLine(new Vector3(_Rect.xMax, 0, _Rect.yMin), new Vector3(_Rect.xMax, 0, _Rect.yMax), color, time);
        }

        public bool GetNode(Vector2 position, ref QuadTreeNode node) {
            if (_Childs == null) {
                if (_Rect.Contains(position)) {
                    node = this;

                    return true;
                }

                return false;
            }
            else {
                foreach (QuadTreeNode child in _Childs)
                    if (child.GetNode(position, ref node))
                        return true;
            }

            return false;
        }

        public void AddAgent(QuadTreeAgent agent) {
            _Agents.Add(agent);
        }

        public void RemoveAgent(QuadTreeAgent agent) {
            _Agents.Remove(agent);
        }
    }

    public class QuadTree {

        private static QuadTree _Current;

        private QuadTreeNode _Root;
        private QuadTreeNode[][] _Nodes;
        private List<QuadTreeAgent> _Result;
        private float _NodeSize;
        private float _Width;
        private float _Height;
        private int _NodesWidth;
        private int _NodesHeight;
        private int _NodesWidthMinusOne;
        private int _NodesHeightMinusOne;

        public static QuadTree current { get { return _Current; } }

        public List<QuadTreeAgent> result { get { return _Result; } }
        public float nodeSize { get { return _NodeSize; } }

        public List<QuadTreeAgent> GetAgents(Vector2 position, float radius) {
            Rect rect = new Rect(position.x - radius / 2f, position.y - radius / 2f, radius, radius);

            return GetAgents(rect);
        }

        public List<QuadTreeAgent> GetAgents(Rect rect) {
            _Result.Clear();
            _Root.CalculateAgents(rect);

            return _Result;
        }

        public void Recalculate(QuadTreeAgent agent, Vector2 position) {
            QuadTreeNode node = null;
            QuadTreeNode agentNode = agent.quadTreeNode;

            node = GetGameQuadNode(position.x, position.y);

            if (node != null) {
                if (agentNode != node) {
                    if (agentNode != null)
                        agentNode.RemoveAgent(agent);

                    node.AddAgent(agent);
                    agent.quadTreeNode = node;
                }
            }
        }

        public void RemoveAgent(QuadTreeAgent agent) {
            QuadTreeNode node = agent.quadTreeNode;

            if (node != null)
                node.RemoveAgent(agent);
        }

        public void SetGameQuadNode(QuadTreeNode node, float x, float y) {
            int dx = (int)(x / _NodeSize);
            int dy = (int)(y / _NodeSize);

            SetGameQuadNode(node, dx, dy);
        }

        public void SetGameQuadNode(QuadTreeNode node, int x, int y) {
            x = Mathf.Clamp(x, 0, _NodesWidthMinusOne);
            y = Mathf.Clamp(y, 0, _NodesWidthMinusOne);

            _Nodes[x][y] = node;
        }

        public QuadTreeNode GetGameQuadNode(float x, float y) {
            int dx = (int)(x / _NodeSize);
            int dy = (int)(y / _NodeSize);

            return GetGameQuadNode(dx, dy);
        }

        public QuadTreeNode GetGameQuadNode(int x, int y) {
            x = Mathf.Clamp(x, 0, _NodesWidthMinusOne);
            y = Mathf.Clamp(y, 0, _NodesWidthMinusOne);

            return _Nodes[x][y];
        }

        public void Recalculate() {
            Rect rect = new Rect(0, 0, _Width, _Height);

            _NodesWidth = (int)(_Width / _NodeSize);
            _NodesHeight = (int)(_Height / _NodeSize);
            _NodesWidthMinusOne = _NodesWidth - 1;
            _NodesHeightMinusOne = _NodesHeight - 1;

            _Nodes = new QuadTreeNode[_NodesWidth][];

            for (int i = 0; i < _NodesWidth; i++)
                _Nodes[i] = new QuadTreeNode[_NodesHeight];

            _Root = new QuadTreeNode(null, rect);
        }

        public QuadTree(float x, float y, float width, float height, float nodeSize) {
            _Current = this;
            _Width = width;
            _Height = height;
            _NodeSize = nodeSize;
            _Result = new List<QuadTreeAgent>(32);

            Recalculate();
        }
    }
}
